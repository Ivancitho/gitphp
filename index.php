<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Recetas Pal Vicente</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- owl css -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- responsive-->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- awesome fontfamily -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/indexcss.css">

</head>
<!-- body -->

<body class="main-layout" style="background: #212020;">
    <!-- loader  -->
    <div class="loader_bg">
        <div class="loader"><img src="imagenes/loading.gif" alt="" /></div>
    </div>

    <div class="wrapper">
    <!-- end loader -->

     <div class="sidebar">
            <!-- Sidebar  -->
            <nav id="sidebar">
            <!--PONER TODO EN ESPAÑOL-->
                <div id="dismiss">
                    <i class="fa fa-arrow-left"></i>
                </div>
                <ul class="list-unstyled components">

                    <li class="active">
<!--                         Hacer una pagina donde un usuario registrador puede mandar un mensaje a un administrador -->
                        <a href="Mensaje.php">Mensaje</a>
                    </li>
                    <li>
<!--                         Aqui se veran TODAS las recetas -->
                        <a href="Recetas.php">Recetas</a>
                    </li>
                    <li>
                        <a href="Contacto.php">Contactanos</a>
                    </li>
                    <li>
                        <a href="CS.php">Cerrar Sesion</a>
                    </li>
                </ul>
            </nav>
        </div>

    <div id="content">
    <!-- header -->
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="full">
                        <a class="logo" href="index.php"><img src="imagenes/rpv.png"/></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="full">
                        <div class="right_header_info">
                            <div style="
                                text-align:right;
                                font-size: 18px;
                                color: white;
                            ">
                            <?php
                                session_start();
                            ?>
                               
                            <?php
                                if (isset($_SESSION['usuario'])){
                                    ?>
                                    <a style="color: #d8b00f;" href=<?php echo"perfil.php"?>>
                                    
                                    <?php
                                    echo $_SESSION['usuario'];?>
                                    </a>
                                    <?php
                                }else{?>
                                    <li class="button_user"><a href="login.php">Login</a>
                                    <a class="button" href="registro.php">Registro</a></li>
                                <?php
                                }
                            ?>
                            </div>
                            <ul>
                                <li>
                                    <button type="button" id="sidebarCollapse">
                                        <img src="imagenes/menu_icon.png">
                                    </button>
                                </li>
                            </ul>
<!--                             <ul>
                                <form action="buscador.php" method="POST">
                                    <input type="text" id="buscar" name="buscar" size="30" maxlength="30" placeholder="Buscador de Recetas">
                                    <input type="image" src='images/search_icon.png' name="busqueda" value="guardar" />
                                </form>
                                <form action="buscadorIngre.php" method="POST">
                                    <input type="text" id="buscarIng" name="buscarIng" size="30" maxlength="30" placeholder="Buscador por Ingredientes">
                                    <input type="image" src='images/search_icon.png' name="busqueda" value="guardar" />
                                </form>
                            </ul> -->
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<br>
<br>
<br>
<br>
<br>
<br>
    <!-- section -->
<section class="resip_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php 
                $host_db = "localhost";
                $user_db = "root";
                $pass_db = "";
                $db_name = "practica2ivansanjose";
                $tbl_name = "recetas";
                     
                $mysqli = new mysqli($host_db, $user_db, $pass_db, $db_name);
                     
                $sql = "SELECT Titulo, Tipo_receta, Dificultad, Fotografia FROM $tbl_name WHERE IdReceta <= 4";
                     
                $result = $mysqli->query($sql);
                     
                $_SESSION['resultadoRecetas'] = $result;

                echo "<br>";
                echo "<p>Mejores Recetas</p>";
                echo "<br>";
                echo "<br>";

                echo "<div class='flex-container'>";

                if(isset($_SESSION['resultadoRecetas'])){
                        while($row = $result->fetch_array(MYSQLI_ASSOC)){
                            echo "<div class='caja'>";
                                echo "<div>";
                                    echo "<div>" . $row["Titulo"] . "</div>";
                                echo "</div>";
                                echo "<br>";
                                echo "<div>";
                                    echo "<div>" . $row["Tipo_receta"] . "</div>";
                                echo "</div>";
                                echo "<br>";
                                echo "<div>";
                                    echo "<div>" . $row["Dificultad"] . "</div>";
                                echo "</div>";
                                echo "<br>";
                                echo "<div>";
                                    ?>
                                        <img src="<?php echo $row["Fotografia"];?>" width="200" height="150px">
                                    <?php
                                echo "</div>";
                            echo "</div>";
                        }
                }
                echo "</div>";
            ?>
            </div>
        </div>
    </div>
</section>
    <!-- footer -->
    <footer>
        <div class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer_logo">
                          <a href="index.html"><img src="imagenes/rpv footer.png" alt="logo" /></a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <ul class="lik">
                            <li class="active"> <a href="index.php">Inicio</a></li>
                            <li> <a href="Recetas.php">Recetas</a></li>
                            <li> <a href="contact.php">Contactanos</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <br>
                        <div align="center">
                            <a class="dinone">Informacion : <img style="margin-right: 15px;margin-left: 15px;" src="imagenes/phone_icon.png"><a>(+34) 776 352 912</a></a>
                            <a class="dinone"><img style="margin-right: 15px;" src="imagenes/mail_icon.png"><a>rpv@gmail.com</a></a><br>
                            <a class="dinone"><img style="margin-right: 15px;height: 21px;position: relative;top: -2px;" src="imagenes/location_icon.png"><a href="https://www.google.com/maps/place/Colegio+Montessori/@41.6475731,-0.8961311,15z/data=!4m9!1m2!2m1!1smontessori!3m5!1s0xd5bfbf010d972fb:0x45d4817a000bc212!8m2!3d41.6452269!4d-0.8891689!15sCgptb250ZXNzb3JpWgwiCm1vbnRlc3NvcmmSARFtb250ZXNzb3JpX3NjaG9vbJoBJENoZERTVWhOTUc5blMwVkpRMEZuU1VSNWFXVnRaVFYzUlJBQg">C. de Mariano Lagasca, 25, 50006 Zaragoza</a></a>
                        </div>
                        <div class="new">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->

    </div>
    </div>
    <div class="overlay"></div>
    <!-- Javascript files-->
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
     <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    
     <script src="js/jquery-3.0.0.min.js"></script>
   <script type="text/javascript">
        $(document).ready(function() {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function() {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>

    <style>
    #owl-demo .item{
        margin: 3px;
    }
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>

     
      <script>
         $(document).ready(function() {
           var owl = $('.owl-carousel');
           owl.owlCarousel({
             margin: 10,
             nav: true,
             loop: true,
             responsive: {
               0: {
                 items: 1
               },
               600: {
                 items: 2
               },
               1000: {
                 items: 5
               }
             }
           })
         })
      </script>

</body>

</html>