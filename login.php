<!DOCTYPE html>
<html lang="en">

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Recetas Pal Vicente</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- owl css -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- responsive-->
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<!-- body -->

<body class="main-layout" style="background: #d8b00f;">
    <!-- loader  -->
    <div class="loader_bg">
        <div class="loader"><img src="imagenes/loading.gif"/></div>
    </div>

    <!-- header -->
    <header>
    <div class="col-md-12">
            <div >
                <a class="logo" href="index.php"><img src="imagenes/rpv.png"></a>
            </div>
     </div>
    </header>
    <br>
    <br>
    <br>
    <!-- end header -->
<section class="resip_section">
    <div class="container">
        <div class="col-md-12">
        <?php
        session_start();
        
        if (isset($_SESSION['usuario'])){ 
            echo "Bienvenido! " . $_SESSION['usuario'];
            echo "<br>";echo "<br>";
            header("Location: index.php");
        }else{ ?>    
        <div >
        <form action="Clogin.php" method="post">
            <label style="color: #d8b00f;">Usuario:</label><br>
            <input name="usuario" id="usuario" required="" type="text">
            <br><br>
            <label style="color: #d8b00f;">Contraseña:</label><br>
            <input name="contraseña" id="contraseña" required="" type="password">
            <br><br>
            <input name="Submit" value="LOGIN" type="submit">
        </form>
        <br>
        <a style="color: #d8b00f;" href=<?php echo"registro.php"?> >Accede al registro</a>
        </div>
        <?php   
        }
        ?>
    </div>
    </div>
    </section>
    <!-- footer -->
    <footer style="    
    background: #212020;
    margin-top: 0;
    padding: 60x 0px 50px 0px;
    border-top: solid rgba(0,0,0,0) 1px;">
        <div class="footer">
        <div class="col-md-12">
            <div class="footer_logo">
                <a href="index.php"><img src="imagenes/rpv footer.png"></a>
            </div>
        </div>
        <div class="col-md-12">
            <ul class="lik">
                <li class="active"> <a href="index.php">Home</a></li>
                <li> <a href="about.html">About</a></li>
                <li> <a href="recipe.html">Recipe</a></li>
                <li> <a href="blog.html">Blog</a></li>
                <li> <a href="contact.html">Contact us</a></li>
            </ul>
        </div>
        <div class="col-md-12">
        <br>
            <div align="center">
                <a class="dinone">Contact Us : <img style="margin-right: 15px;margin-left: 15px;" src="imagenes/phone_icon.png"><a>(+34) 776 352 912</a></a>
                <a class="dinone"><img style="margin-right: 15px;" src="imagenes/mail_icon.png"><a>rpv@gmail.com</a></a><br>
                <a class="dinone"><img style="margin-right: 15px;height: 21px;position: relative;top: -2px;" src="imagenes/location_icon.png"><a href="https://www.google.com/maps/place/Colegio+Montessori/@41.6475731,-0.8961311,15z/data=!4m9!1m2!2m1!1smontessori!3m5!1s0xd5bfbf010d972fb:0x45d4817a000bc212!8m2!3d41.6452269!4d-0.8891689!15sCgptb250ZXNzb3JpWgwiCm1vbnRlc3NvcmmSARFtb250ZXNzb3JpX3NjaG9vbJoBJENoZERTVWhOTUc5blMwVkpRMEZuU1VSNWFXVnRaVFYzUlJBQg">C. de Mariano Lagasca, 25, 50006 Zaragoza</a></a>
            </div>
         <br>
        </div>
        </div>
    </footer>
    
    <!-- end footer -->
    <!-- Javascript files-->
    <script src="js/jquery.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
</body>

</html>